<?php
App::uses('Controller', 'Controller');


class AppController extends Controller {

    public $components = array('Flash',
        'Auth' => array(
            'loginRedirect' => array('controller' => 'posts', 'action' => 'index'),
            'logoutRedirect' => array('controller' => 'pages', 'action' => 'display', 'home'),
            'authenticate' => array(
                'all' => array(),'Basic'
            )
        )
      );



    public function isAuthorized($user) {
        if (isset($user['role']) && ($user['role'] == 'admin')) {
            return true;
        }
        return false;
    }

}
