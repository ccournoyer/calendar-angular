<?php

App::uses('AppController', 'Controller');
App::uses('AuthComponent', 'Controller/Component');

class UsersController extends AppController {


  public $components = array('Paginator', 'RequestHandler',"Email","Session");
  public $helpers=array("Html","Form","Session");

  public function beforeFilter() {
    parent::beforeFilter();

    $this->Auth->allow('login','add','is_logged','logout','forgetpwd','reset');
  }

  public function login() {
    if ($this->Auth->user()) {
      $this->set(array(
        'message' => array(
          'text' => __('You are logged in!'),
          'type' => 'error'
        ),
        '_serialize' => array('message')
      ));
    }

    if ($this->request->is('get')) {
      if ($this->Auth->login()) {

        $this->set(array(
          'user' => $this->Auth->user(),
          '_serialize' => array('user')
        ));

      } else {

        $this->set(array(
          'message' => array(
            'text' => __('Invalid username or password, try again'),
            'type' => 'error'
          ),
          '_serialize' => array('message')
        ));
        $this->response->statusCode(401);

      }
    }

  }

  public function logout() {
    if ($this->Auth->logout()) {
      $this->set(array(
        'message' => array(
          'text' => __('Logout successfully'),
          'type' => 'info'
        ),
        '_serialize' => array('message')
      ));

    }
  }


  public function index($offset=0) {
    //$User = $this->User->query("SELECT * FROM users WHERE role <> 'admin' or role is NULL");
    $User = $this->User->find('all', array(
      'conditions' => array('role  != '=>'admin'
      ),'limit' => 20,'offset' => $offset,
       'order' => array('created' => 'desc')
  ));
  $number = $this->User->find('count', array(
    'conditions' => array('role  != '=>'admin'
    ),'limit' => 20,'offset' => $offset
));
    $this->set(array(
      'Users' => $User,
      'Total' =>$number,
      '_serialize' => array('Users','Total')
    ));
  }

  public function view($id = null) {
    if (!$this->User->exists($id)) {
      throw new NotFoundException(__('Invalid user'));
    }

    $User = $this->User->findById($id);
    $this->set(array(
      'Users' => $User,
      '_serialize' => array('Users')
    ));
  }

  public function catchme($seach="_all",$offset=0) {
    $offset=intval($offset);
    $User = $this->User->find('all', array(
      'conditions' => array('role  != '=>'admin'
    ),'limit' => 20,'offset' => $offset,
     'order' => array('created' => 'desc')
  ));
  $number = $this->User->find('count', array(
    'conditions' => array('role  != '=>'admin'
  ),'limit' => 20,'offset' => $offset
));

    if($seach !="_all"){
      $User = $this->User->find('all', array(
        'conditions' => array('role  != '=>'admin',
        'OR' => array(
          array('first_name LIKE' => '%'.$seach.'%'),
          array('last_name LIKE' => '%'.$seach.'%'),
          array('id ' => $seach)
        )
      ),'limit' => 20,'offset' => $offset,
       'order' => array('created' => 'desc')
    ));
    $number = $this->User->find('count', array(
      'conditions' => array('role  != '=>'admin',
      'OR' => array(
        array('first_name LIKE' => '%'.$seach.'%'),
        array('last_name LIKE' => '%'.$seach.'%'),
        array('id ' => $seach)
      ))
    ));
    }

  $this->set(array(
    'Users' => $User,
    'Total' => $number,
    'Message' => 'Now you see me',
    '_serialize' => array('Users','Total','Message')
  ));
}


public function add() {
  if ($this->request->is('post')) {
    $dataname = $this->request->input('json_decode');
    $conditions = array('username' => $dataname->username);
    $condition2s = array('email' => $dataname->email);
    if ($this->User->hasAny($conditions)){
      //throw new NotFoundException(__('Username exit'));
      $this->set(array(
        'message' => array(
          'text' => __('The username exist. Please, try again with a different username.'),
          'type' => 'error',
          'error_type'=>'username'
        ),
        '_serialize' => array('message')
      ));
      //$this->response->statusCode(400);
    }

    else if ($this->User->hasAny($condition2s)){
      //throw new NotFoundException(__('Username exit'));
      $this->set(array(
        'message' => array(
          'text' => __('The email exist. Please, try again with a different email.'),
          'type' => 'error',
          'error_type'=>'email'
        ),
        '_serialize' => array('message')
      ));
      //$this->response->statusCode(400);
    }else{
      $this->User->create();
      if ($this->User->save($this->request->data)) {
        $last = $this->User->id;
        $this->set(array(
          'message' => array(
            'text' => __('Registered successfully'),
            'type' => 'success',
            'data_id' => $last

          ),
          '_serialize' => array('message')
        ));
      } else {
        $this->set(array(
          'message' => array(
            'text' => __('The user could not be saved. Please, try again.'),
            'type' => 'error',
            'error_type'=>'unknown'
          ),
          '_serialize' => array('message')
        ));
        $this->response->statusCode(400);
      }
    }
  }else{
    $this->set(array(
      'message' => array(
        'text' => __('No data was posted. Please, try again.'),
        'type' => 'error'
      ),
      '_serialize' => array('message')
    ));
    $this->response->statusCode(400);
  }
}


public function is_logged(){
  if($this->Auth->user('id')){
    $User = $this->Auth->user('id');
    $User = $this->User->findById($User);
    $this->set(array(
      'Users' => $User,
      '_serialize' => array('Users')
    ));
  }else{
    $this->set(array(
      'Users' => false,
      '_serialize' => array('Users')
    ));
  }
}


public function edit($id = null) {
  if (!$this->User->exists($id)) {
    throw new NotFoundException(__('Invalid user'));
  }
  $this->User->id = $id;
  if ($this->User->save($this->request->data)) {
    $message = array(
      'text' => __('User saved'),
      'type' => 'success'
    );
  } else {
    $message = array(
      'text' => __('Error user could not be saved'),
      'type' => 'error'
    );
  }
  $this->set(array(
    'message' => $message,
    '_serialize' => array('message')
  ));
}


public function delete($id) {
    $this->loadModel('Evaluation');

    if ($this->User->delete($id)) {
      $this->deleteDir($id);
      $Evaluations = $this->Evaluation->find('all', array(
        'conditions' => array('user_id' => $id)
      ));

      $i=0;
      foreach ($Evaluations as $Evaluation) {
        $this->Evaluation->delete($Evaluation['Evaluation']['id']);
        $i++;
      }

      $message = array(
        'text' => __('Deleted'),
        'type' => 'success',
        'Evaluation_supprimer'=>$i
      );
    } else {
      $message = array(
        'text' => __('Error'),
        'type' => 'error'
      );
    }
    $this->set(array(
      'message' => $message,
      '_serialize' => array('message')
    ));
  }


  private function deleteDir($client_id) {
      $target_dir = "uploads/files/";
      $dirPath= $target_dir.$client_id."/";
      $files = glob($dirPath . '*', GLOB_MARK);


      if (! is_dir($dirPath)) {
          throw new InvalidArgumentException("$dirPath must be a directory");
      }
      if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
          $dirPath .= '/';
      }
      $files = glob($dirPath . '*', GLOB_MARK);
      foreach ($files as $file) {
          if (is_dir($file)) {
              self::deleteDir($file);
          } else {
              unlink($file);
          }
      }
      rmdir($dirPath);
  }


function forgetpwd(){
  //$this->layout="signup";
  //$this->User->recursive=-1;
  if($this->request->is('post')){
    $dataname = $this->request->input('json_decode');
    $email=$dataname->email;
    $fu=$this->User->find('first',array('conditions'=>array('User.email'=>$email)));
    if($fu){
      //debug($fu);
      if($fu['User']['active']){
        $key = Security::hash(CakeText::uuid(),'sha512',true);
        $hash=sha1($fu['User']['username'].rand(0,100));
        //$url = Router::url( array('controller'=>'users','action'=>'reset'), true ).'/'.$key.'#'.$hash;
        //$url = 'http://geva.oktanecms.ca/#/forgotten_password/modify/'.$key.'#'.$hash;
          $url = 'http://geva.oktanecms.ca/#/forgotten_password/modify/'.$key;
        $ms=$url;
        $ms=wordwrap($ms,1000);
        //debug($url);
        $fu['User']['tokenhash']=$key;
        $this->User->id=$fu['User']['id'];
        if($this->User->saveField('tokenhash',$fu['User']['tokenhash'])){

          //============Email================//
          /* SMTP Options */
          /*$this->Email->smtpOptions = array(
          'port'=>'25',
          'timeout'=>'30',
          'host' => 'mail.example.com',
          'username'=>'accounts+example.com',
          'password'=>'your password'
        );*/

        $this->Email->template = 'resetpw';
        $this->Email->from    = 'Geva <Geva@oktanecms.ca>';
        $this->Email->to      = $fu['User']['first_name'].'<'.$fu['User']['email'].'>';
        $this->Email->subject = 'Renouvellement de mot de passe';
        $this->Email->sendAs = 'html';

        //$this->Email->delivery = 'smtp';
        $this->set('ms', $ms);
        $this->set('username', $fu['User']['username']);
        $this->Email->send();
        $this->set('smtp_errors', $this->Email->smtpError);
        $this->set(array(
          'message' => array(
            'text' => __('Check Your Email To Reset your password'),
            'code' =>'success',
            'type' => 'info'
          ),
          '_serialize' => array('message')
        ));
        //============EndEmail=============//
      }
      else{
        //$this->Session->setFlash("Error Generating Reset link");
        $this->set(array(
          'message' => array(
            'text' => __('Error Generating Reset link'),
            'code' =>'error',
            'type' => 'Error'
          ),
          '_serialize' => array('message')
        ));
      }
    }
    else{
      $this->Session->setFlash('This Account is not Active yet.Check Your mail to activate it');
      $this->set(array(
        'message' => array(
          'text' => __('This Account is not Active yet.Check Your mail to activate it'),
          'code' =>'error',
          'type' => 'info'
        ),
        '_serialize' => array('message')
      ));
    }
  }
  else{
    //$this->Session->setFlash('Email does Not Exist');
    $this->set(array(
      'message' => array(
        'text' => __('Email does Not Exist'),
        'code' =>'errormail',
        'type' => 'info'
      ),
      '_serialize' => array('message')
    ));
  }
}else{
  $this->set(array(
    'message' => array(
      'text' => __('Nothing was submitted'),
      'code' =>'no_info',
      'type' => 'info'
    ),
    '_serialize' => array('message')
  ));
}
}

function reset(){
  //$this->layout="Login";
  //$this->User->recursive=-1;
  if($this->request->is('post')){
    $token = $this->request->input('json_decode');
    $password = $token->password;
    $token = $token->token;
    if(!empty($token)){
      $u=$this->User->findBytokenhash($token);
      if($u){
        $this->User->id=$u['User']['id'];
        if(!empty($this->data)){
          $this->User->data=$this->data;

          $this->User->data['User']['username']=$u['User']['username'];
          $this->User->data['User']['password'] = $password;
          $new_hash=sha1($u['User']['username'].rand(0,100));//created token
          $this->User->data['User']['tokenhash']=$new_hash;
          if($this->User->save($this->User->data)){
            //$this->Session->setFlash('Password Has been Updated');
            //$this->redirect(array('controller'=>'users','action'=>'login'));
            $this->set(array(
              'message' => array(
                'text' => __('Password Has been Updated'),
                'code' =>'success',
                'type' => 'info'
              ),
              '_serialize' => array('message')
            ));
          }

        }
      }
      else{
        //$this->Session->setFlash('Token Corrupted,,Please Retry.the reset link work only for once.');
        $this->set(array(
          'message' => array(
            'text' => __('Token Corrupted,Please Retry.the reset link work only for once.'),
            'type' => 'Error'
          ),
          '_serialize' => array('message')
        ));
      }
    }

    else{
      $this->redirect('/');
    }
  }
  else{
    $this->set(array(
      'message' => array(
        'text' => __('Nothing was submitted'),
        'type' => 'info'
      ),
      '_serialize' => array('message')
    ));
  }
}
}
