<p>Vous recevez ce courriel puisqu'une demande de renouvellement de mot de passe a été effectuée pour le compte suivant :<br/>
Identifiant : <?php echo $username; ?><br/>
S'il s'agit d'une erreur, ignorez ce message. La demande ne sera pas prise en compte.<br/>
Si vous souhaitez renouveler votre mot de passe, cliquez sur le lien suivant :
<a href="<?php echo $ms; ?>">Renouveler mon mot de passe</a>
</p>
